import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { MenuController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-qrscan',
  templateUrl: './qrscan.page.html',
  styleUrls: ['./qrscan.page.scss'],
})
export class QrscanPage implements OnInit {
  adtiveText = 'ถังจัดส่ง';

  scannedData = {
    new: [
      // {text:'ถังจัดส่ง 1'},
      // {text:'ถังจัดส่ง 2'},
      // {text:'ถังจัดส่ง 3'},
      // {text:'ถังจัดส่ง 4'},
      // {text:'ถังจัดส่ง 5'}
    ],
    old: [
      // {text:'ถังรับกลับ 1'},
      // {text:'ถังรับกลับ 2'},
      // {text:'ถังรับกลับ 3'},
      // {text:'ถังรับกลับ 4'},
      // {text:'ถังรับกลับ 5'}
    ],
  };
  activeBtn = true;
  encodedData: '';
  encodeData: any;
  inputData: any;
  selectOrder: any;
  dataShow: any;
  dataOrders = {
    Order_no: null,
    Order_date: null,
    status_desc: null,
    Order_qty: null,
    Total_delivery: null,
    Total_deposit: null,
    Total_discount: null,
    Total_amount: null,
    Ship_to_desc: null,
    Latitude: null,
    Longitude: null,
    sale_total_amount: null,
    sale_total_disamount: null,
    Cus_id: null,
    customer_Name: null,
    Max_id: null,
    Order_com: null,
    Company_Name: null,
    Order_plant: null,
    Plant_name_th: null,
    order_dt: [],
  };
  ordersQr: any;

  // qrText = ['00010001003', '00010001004'];
  qrText = [];

  otherBrand: any;
  otherBrandIndex = [];
  checkBoxOtherBrand = false;
  headerName: string = '';
  objOtherBrandIndex: any;

  constructor(
    private barcodeScanner: BarcodeScanner,
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController,
    private geolocation: Geolocation
  ) {
    this.menuCtrl.enable(true);

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //     this.loadOrder();
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });

    let watch = this.geolocation.watchPosition();
  }

  ngOnInit() {}

  ionViewWillEnter() {
    this.storageService
      .getObject('selectPland')
      .then((result) => {
        console.log(result);
        if (result.name2) {
          this.headerName = 'ชื่อร้าน : ' + result.name2;
        } else {
          this.headerName = '';
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('ordersQr')
      .then((result) => {
        if (result != null) {
          this.ordersQr = result;

          for (let i = 0; i < Number(this.ordersQr.Order_qty); i++) {
            this.scannedData.new.push({
              text: 'กดเพื่อสแกนถังจัดส่ง ' + (i + 1),
              text2: 'กรอกข้อมูลถังจัดส่ง ' + (i + 1),
              data: {
                sale_no: '',
                qr_code: '',
                serial_number: '',
                mat_code: '',
                latitude: null,
                longitude: null,
                scan_type: '00',
              },
            });

            this.scannedData.old.push({
              text: 'กดเพื่อสแกนถังรับกลับ ' + (i + 1),
              text2: 'กรอกข้อมูลถังรับกลับ ' + (i + 1),
              data: {
                sale_no: '',
                qr_code: '',
                serial_number: '',
                mat_code: '',
                latitude: null,
                longitude: null,
                scan_type: '01',
              },
            });

            this.otherBrandIndex.push(null);
          }

          console.log(this.ordersQr);
        }

        this.loadQrscan();
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.apiservice
      .GetMaterialForDepositSale()
      .then((response) => {
        console.log(response);
        this.otherBrand = response;

        // this.objOtherBrandIndex = {};

        // for (const key in this.otherBrand) {
        //   this.objOtherBrandIndex[this.otherBrand[key]?.mat_code] = key;
        // }

        this.loadOtherBrandSelect();
      })
      .catch((e) => {
        console.error(e);
      });
  }

  loadQrscan() {
    this.storageService
      .getObject('qrscan')
      .then((result) => {
        if (result) {
          const keyQr = this.ordersQr.Mat_code + '_' + this.ordersQr.Item_no;
          if (result[keyQr]) {
            this.dataShow = result[keyQr].new;
            this.scannedData = result[keyQr];
          } else {
            this.dataShow = this.scannedData.new;
          }
        } else {
          this.dataShow = this.scannedData.new;
        }

        this.loadOrder();
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  loadOtherBrandSelect() {
    this.storageService
      .getObject('otherBrandSelect')
      .then((result) => {
        if (result) {
          const keyOther = this.ordersQr.Mat_code + '_' + this.ordersQr.Item_no;
          if (result[keyOther]) {
            this.otherBrandIndex = result[keyOther];
          }
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  loadOrder() {
    this.storageService
      .getObject('selectOrder')
      .then(async (result) => {
        if (result != null) {
          this.selectOrder = result;

          const loading = await this.loadingctrl.create({
            message: 'Please wait...',
            duration: 0,
          });
          await loading.present();
          this.apiservice.OrderingByOrderId(this.selectOrder).subscribe(
            (res) => {
              const data = JSON.parse(res);
              // console.log(data);
              if (data.response.responseCode === '0000') {
                this.dataOrders = data.data[0];
                // console.log(this.dataOrders);
              } else {
                this.presentToast(data.response.responseMsg);
              }
              loading.dismiss();
            },
            (error) => {
              loading.dismiss();
              console.log(error);
            }
          );
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // this.storageService
    //   .getObject('qrscan')
    //   .then((result) => {
    //     if (result) {
    //       this.scannedData = result;
    //       this.dataShow = this.scannedData.new;
    //     }
    //   })
    //   .catch((e) => {
    //     console.log('error: ', e);
    //   });
  }

  changeTank(activeButton) {
    console.log(this.scannedData);
    if (activeButton) {
      this.adtiveText = 'ถังจัดส่ง';
      this.dataShow = this.scannedData.new;
    } else {
      this.adtiveText = 'ถังรับกลับ';
      this.dataShow = this.scannedData.old;
    }

    this.qrText = [];

    this.activeBtn = activeButton;
  }

  scanBarcode(index) {
    const options: BarcodeScannerOptions = {
      preferFrontCamera: false,
      showFlipCameraButton: true,
      showTorchButton: true,
      torchOn: false,
      prompt: 'Place a barcode inside the scan area',
      resultDisplayDuration: 500,
      formats: 'EAN_13,EAN_8,QR_CODE,PDF_417 ',
      orientation: 'portrait',
    };

    this.barcodeScanner
      .scan(options)
      .then(async (barcodeData) => {
        console.log('Barcode data', barcodeData);

        // if (this.adtiveText === 'ถังจัดส่ง') {
        //   this.scannedData.new[index].data.qr_code = barcodeData.text;
        // } else {
        //   this.scannedData.old[index].data.qr_code = barcodeData.text;
        // }
        if (barcodeData.text !== '') {
          this.getCurrentPosition(index, barcodeData.text);
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });
  }

  getQrtext(index, barcodeData) {
    this.getCurrentPosition(index, barcodeData);
  }

  async getCurrentPosition(index, barcodeData) {
    
    // console.log(index, barcodeData);
    // console.log(this.dataShow);
    // console.log(this.selectOrder);

    if(barcodeData.length > 10){
      barcodeData = barcodeData.slice(-10);
    }

    this.GetMaterialFromScan(index, barcodeData, { latitude: this.selectOrder.Latitude, longitude: this.selectOrder.Longitude });

    // const loading = await this.loadingctrl.create({
    //   message: 'Please wait...',
    //   duration: 0,
    // });
    // await loading.present();
    // this.geolocation
    //   .getCurrentPosition()
    //   .then((resp) => {
    //     // resp.coords.latitude
    //     // resp.coords.longitude
    //     console.log('geolocation', resp.coords.latitude + ' ' + resp.coords.longitude);
    //     // if (this.adtiveText === 'ถังจัดส่ง') {
    //     //   this.scannedData.new[index].data.qr_code = barcodeData;
    //     //   this.scannedData.new[index].data.latitude = resp.coords.latitude;
    //     //   this.scannedData.new[index].data.longitude = resp.coords.longitude;
    //     // } else {
    //     //   this.scannedData.old[index].data.qr_code = barcodeData;
    //     //   this.scannedData.old[index].data.latitude = resp.coords.latitude;
    //     //   this.scannedData.old[index].data.longitude = resp.coords.longitude;
    //     // }
    //     loading.dismiss();
    //     this.GetMaterialFromScan(index, barcodeData, resp.coords);
    //   })
    //   .catch((error) => {
    //     loading.dismiss();
    //     console.log('Error getting location', error);
    //   });
  }

  async GetMaterialFromScan(index, barcodeData, coords) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();
    this.apiservice
      .GetMaterialFromScan(barcodeData)
      .then((res: any) => {
        const data = JSON.parse(res);

        console.log(data);
        // console.log(this.ordersQr?.Mat_code, data?.data?.Mat_code);

        if (this.ordersQr?.Mat_code === data?.data?.Mat_code) {
          if (data?.data?.Serial_number !== undefined) {
            if (this.adtiveText === 'ถังจัดส่ง') {
              this.scannedData.new[index].text = this.adtiveText + (index + 1) + ' QR Code : ';
              this.scannedData.new[index].data.qr_code = barcodeData;
              this.scannedData.new[index].data.latitude = coords.latitude;
              this.scannedData.new[index].data.longitude = coords.longitude;
              this.scannedData.new[index].data.serial_number = data.data.Serial_number;
              this.scannedData.new[index].data.mat_code = data.data.Mat_code;

              this.scannedData.new[index].data.Vendor_description = data.data.Vendor_description;
              this.scannedData.new[index].data.Serial_number = data.data.Serial_number;
              this.scannedData.new[index].data.Mat_desc = data.data.Mat_desc;
              this.scannedData.new[index].data.Mfg_date = data.data.Mfg_date;

              this.dataShow = this.scannedData.new;
            } else {
              this.scannedData.old[index].text = this.adtiveText + (index + 1) + ' QR Code : ';
              this.scannedData.old[index].data.qr_code = barcodeData;
              this.scannedData.old[index].data.latitude = coords.latitude;
              this.scannedData.old[index].data.longitude = coords.longitude;
              this.scannedData.old[index].data.serial_number = data.data.Serial_number;
              this.scannedData.old[index].data.mat_code = data.data.Mat_code;

              this.scannedData.old[index].data.Vendor_description = data.data.Vendor_description;
              this.scannedData.old[index].data.Serial_number = data.data.Serial_number;
              this.scannedData.old[index].data.Mat_desc = data.data.Mat_desc;
              this.scannedData.old[index].data.Mfg_date = data.data.Mfg_date;

              this.dataShow = this.scannedData.old;
            }
          } else {
            this.presentToast('ไม่พบข้อมูลถัง');
          }
        } else {
          this.presentToast('ข้อมูลถังไม่ถูกต้อง');
        }
        // console.log('dataShow', this.dataShow);
        loading.dismiss();
      })
      .catch((error) => {
        loading.dismiss();
        this.presentToast('ข้อมูลถังไม่ถูกต้อง');
        console.log(error);
      });
  }

  saveQrscan() {
    // console.log(this.checkBoxOtherBrand);
    // console.log(this.otherBrandIndex);

    this.storageService
      .getObject('qrscan')
      .then((result) => {
        let qrscan = result;
        if (!result) {
          qrscan = {};
        }
        const keyQr = this.ordersQr.Mat_code + '_' + this.ordersQr.Item_no;
        qrscan[keyQr] = this.scannedData;

        this.storageService.setObject('qrscan', qrscan);

        // // if (this.checkBoxOtherBrand) {
        // let tmpOtherBrandSelect = [];
        // this.otherBrandIndex.forEach((id) => {
        //   if (id !== null) {
        //     tmpOtherBrandSelect.push({
        //       // ref_no: '',
        //       item_no: '',
        //       mat_code: this.otherBrand[id]?.mat_code,
        //       base_unit: this.otherBrand[id]?.base_unit,
        //       return_qty: 1,
        //       return_price: this.otherBrand[id]?.dep_price,
        //       return_type: 'SALE',
        //       return_ref: '',
        //       return_value: this.otherBrand[id]?.dep_price,
        //     });
        //   }
        // });

        // console.log('---------------------', tmpOtherBrandSelect);

        // this.storageService.setObject('otherBrandSelect', tmpOtherBrandSelect);

        // // let tmpOtherBrandSelect = [];
        // // this.storageService
        // //   .getObject('otherBrandSelect')
        // //   .then((resultOther) => {
        // //     if (resultOther != null) {
        // //       tmpOtherBrandSelect = resultOther;
        // //       // console.log(this.ordersQr);
        // //     }

        // //     this.otherBrandIndex.forEach((element) => {
        // //       tmpOtherBrandSelect.push(element);
        // //     });

        // //     this.storageService.setObject('otherBrandSelect', tmpOtherBrandSelect);

        // //     this.router.navigateByUrl('/orders');
        // //   })
        // //   .catch((eOther) => {
        // //     console.log('error: ', eOther);
        // //   });
        // // } else {
        // //   this.router.navigateByUrl('/orders');
        // // }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('otherBrandSelect')
      .then((result) => {
        let otherBrandSelect = result;
        if (!result) {
          otherBrandSelect = {};
        }
        const keyOther = this.ordersQr.Mat_code + '_' + this.ordersQr.Item_no;
        otherBrandSelect[keyOther] = this.otherBrandIndex;

        this.storageService.setObject('otherBrandSelect', otherBrandSelect);

        // // if (this.checkBoxOtherBrand) {
        // let tmpOtherBrandSelect = [];
        // this.otherBrandIndex.forEach((id) => {
        //   if (id !== null) {
        //     tmpOtherBrandSelect.push({
        //       // ref_no: '',
        //       item_no: '',
        //       mat_code: this.otherBrand[id]?.mat_code,
        //       base_unit: this.otherBrand[id]?.base_unit,
        //       return_qty: 1,
        //       return_price: this.otherBrand[id]?.dep_price,
        //       return_type: 'SALE',
        //       return_ref: '',
        //       return_value: this.otherBrand[id]?.dep_price,
        //     });
        //   }
        // });

        // this.storageService.setObject('otherBrandSelect', tmpOtherBrandSelect);
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    setTimeout(() => {
      this.router.navigateByUrl('/orders');
    }, 500);
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
