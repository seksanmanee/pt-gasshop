import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then((m) => m.TabsPageModule),
  },
  {
    path: 'qrscan',
    loadChildren: () => import('./qrscan/qrscan.module').then((m) => m.QrscanPageModule),
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then((m) => m.RegisterPageModule),
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then((m) => m.ForgotpasswordPageModule),
  },
  {
    path: 'order-payment',
    loadChildren: () => import('./order-payment/order-payment.module').then((m) => m.OrderPaymentPageModule),
  },
  {
    path: 'orders',
    loadChildren: () => import('./orders/orders.module').then((m) => m.OrdersPageModule),
  },
  {
    path: 'print-slip',
    loadChildren: () => import('./print-slip/print-slip.module').then((m) => m.PrintSlipPageModule),
  },
  {
    path: 'order-direction-map',
    loadChildren: () => import('./order-direction-map/order-direction-map.module').then((m) => m.OrderDirectionMapPageModule),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
