import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, AlertController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { GoogleBasemap } from '../class/google-basemap';
import { Geolocation } from '@ionic-native/geolocation/ngx';
declare const google: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  GoogleBasemap = new GoogleBasemap();
  trafficLayer: any;
  map: any;

  userData: any;
  companyData: any;
  userImg = '';
  arrArticles: any;
  arrVideos: any;
  dataPlands: any;
  dataPland: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    public apiservice: ApiService,
    private geolocation: Geolocation
  ) {
    // this.storageService.getObject('logined').then(result => {
    //   if (result != null) {
    //     this.userData = result;
    //     console.log();
    //   }
    // }).catch(e => {
    //   console.log('error: ', e);
    // });

    let watch = this.geolocation.watchPosition();
  }

  ngOnInit() {
    this.menuCtrl.enable(true);

    this.apiservice.GetCompanyByUser().subscribe(
      (res) => {
        // console.log(res);
        // const data = JSON.parse(res);
        this.dataPlands = res.singleroledata;
        this.dataPland = res.singleroledata[0];

        //console.log(this.dataPlands);

        this.storageService.setObject('company_data', this.dataPlands);
        localStorage.setItem('company_data', JSON.stringify(this.dataPlands));
      },
      (error) => {
        console.log(error);
      }
    );
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          console.log(this.userData);
          this.getarticlebookmark();
          this.getVideobookmark();
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('company_data')
      .then((result) => {
        if (result != null) {
          this.companyData = result[0];
          // console.log(this.companyData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // this.map = this.GoogleBasemap.initMap('mapGoogle');
    // this.trafficLayer = new google.maps.TrafficLayer();

    // this.geolocation.getCurrentPosition().then((resp) => {
    //   // resp.coords.latitude
    //   // resp.coords.longitude
    //   // console.log("geolocation", resp.coords.latitude);
    //   this.addMarker(resp.coords);

    //  }).catch((error) => {
    //    console.log('Error getting location', error);
    //  });

    // this.addMarker({ latitude: 13.6634393, longitude: 100.3501419 });
  }

  // addMarker(coords) {
  //   const image = {
  //     url: '../../assets/img/pinpt.png',
  //     // This marker is 20 pixels wide by 32 pixels high.
  //     size: new google.maps.Size(40, 48),
  //     // The origin for this image is (0, 0).
  //     origin: new google.maps.Point(0, 0),
  //     // The anchor for this image is the base of the flagpole at (0, 32).
  //     anchor: new google.maps.Point(24, 48),
  //   };

  //   let marker = new google.maps.Marker({
  //     icon: image,
  //     map: this.map,
  //     // draggable: true,
  //     animation: google.maps.Animation.DROP,
  //     position: { lat: coords.latitude, lng: coords.longitude },
  //   });

  //   marker.addListener('dragend', (event) => {
  //     console.log(event.latLng.lat() + ',' + event.latLng.lng());
  //   });

  //   this.map.setCenter(marker.getPosition());
  //   this.map.setZoom(12);
  // }

  editProfile() {
    this.router.navigateByUrl('/editprofile');
  }

  seeArticleDetail(id: string) {
    this.nav.navigateForward('/articledetail/' + id);
  }

  seeVideoDetail(id: string) {
    this.nav.navigateForward('/videobookmark/' + id);
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure you want to logout ?',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'Okay',
          handler: () => {
            this.storageService.clear();
            this.router.navigateByUrl('/welcome');
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }

  getarticlebookmark() {}

  getVideobookmark() {}
}
