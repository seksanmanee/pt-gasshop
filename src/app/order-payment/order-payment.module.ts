import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPaymentPageRoutingModule } from './order-payment-routing.module';

import { OrderPaymentPage } from './order-payment.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPaymentPageRoutingModule,
    ExploreContainerComponentModule
  ],
  declarations: [OrderPaymentPage]
})
export class OrderPaymentPageModule {}
