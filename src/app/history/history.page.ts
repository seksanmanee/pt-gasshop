import { Component, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { MenuController, NavController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  // userData: any;
  dataOrders: any;

  dataPland: any;
  selectPland: any;
  plandActive: any;

  headerName: string = '';

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public storageService: StorageService,
    public toastController: ToastController
  ) {
    this.menuCtrl.enable(true);
    // this.storageService.getObject('logined').then(result => {
    //   if (result != null) {
    //     console.log(result);
    //     // this.userData = result;
    //     // console.log( this.userData);
    //   }
    // }).catch(e => {
    //   console.log('error: ', e);
    // });
    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //     if (this.plandActive) {
    //       this.getOrderList(this.plandActive);
    //     }
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  async ngOnInit() {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();

    setTimeout(() => {
      this.apiservice.GetCompanyByUser().subscribe(
        (res) => {
          // console.log(res);
          // const data = JSON.parse(res);
          this.dataPland = res.singleroledata;

          this.storageService.setObject('company_data', this.dataPland);
          localStorage.setItem('company_data', JSON.stringify(this.dataPland));

          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }, 500);
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('selectPland')
      .then((result) => {
        console.log(result);
        if (result) {
          this.headerName = 'ชื่อร้าน : ' + result.name2;
          this.plandActive = result.sin_cs_plant;
          this.selectPland = result.sin_cs_plant;
          this.getOrderList(this.plandActive);
        } else {
          this.headerName = '';
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  onPlantChange(plant) {
    this.plandActive = plant;

    for (const key in this.dataPland) {
      if (this.dataPland[key].sin_cs_plant == this.plandActive) {
        this.headerName = 'ชื่อร้าน : ' + this.dataPland[key].name2;
        this.storageService.setObject('selectPland', this.dataPland[key]);
      }
    }

    this.getOrderList(this.plandActive);
  }

  async getOrderList(plant) {
    this.dataOrders = [];
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });

    await loading.present();

    setTimeout(() => {
      this.apiservice.OrderList(plant).subscribe(
        (res) => {
          const data = JSON.parse(res);
          if (data.response.responseCode === '0000') {
            const tmpData = [];
            for (const key in data.data) {
              if (data.data[key].Status_code !== 'N') {
                tmpData.push(data.data[key]);
              }
            }
            this.dataOrders = tmpData;
            // console.log(this.dataOrders);
          } else {
            this.presentToast(data.response.responseMsg);
          }
          loading.dismiss();
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }, 500);
  }

  convertStatusText(status) {
    let statusText = '';
    switch (status) {
      case 'N':
        statusText = 'รอยืนยันรายการสั่งซื้อ';
        break;
      case 'W':
        statusText = 'อยู่ระหว่างการจัดส่ง';
        break;
      case 'D':
        statusText = 'จัดส่งเรียบร้อย';
        break;
      case 'S':
        statusText = 'ชำระเงินเรียบร้อย';
        break;
      case 'C':
        statusText = 'ยกเลิก';
        break;
    }
    return statusText;
  }

  gotoDetail(selectOrder) {
    this.storageService.remove('qrscan');
    this.storageService.setObject('selectOrder', selectOrder);
    setTimeout(() => {
      this.router.navigateByUrl('/orders');
    }, 100);
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
