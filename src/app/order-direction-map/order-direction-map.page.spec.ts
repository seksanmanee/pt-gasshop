import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderDirectionMapPage } from './order-direction-map.page';

describe('OrderDirectionMapPage', () => {
  let component: OrderDirectionMapPage;
  let fixture: ComponentFixture<OrderDirectionMapPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderDirectionMapPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderDirectionMapPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
