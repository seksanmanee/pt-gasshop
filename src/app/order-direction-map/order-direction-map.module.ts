import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDirectionMapPageRoutingModule } from './order-direction-map-routing.module';

import { OrderDirectionMapPage } from './order-direction-map.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderDirectionMapPageRoutingModule
  ],
  declarations: [OrderDirectionMapPage]
})
export class OrderDirectionMapPageModule {}
