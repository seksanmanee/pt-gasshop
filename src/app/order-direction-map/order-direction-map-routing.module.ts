import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderDirectionMapPage } from './order-direction-map.page';

const routes: Routes = [
  {
    path: '',
    component: OrderDirectionMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderDirectionMapPageRoutingModule {}
