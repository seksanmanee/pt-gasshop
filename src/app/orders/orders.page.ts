import { Component, OnChanges, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit, OnChanges {
  selectOrder: any;
  product: any;
  userData: any;
  arrArticles: any;
  showPriceDetail = false;
  dataOrders = {
    Order_no: null,
    Order_date: null,
    Status_desc: null,
    Order_qty: null,
    Total_delivery: null,
    Total_deposit: null,
    Total_discount: null,
    Total_amount: null,
    Ship_to_desc: null,
    Latitude: null,
    Longitude: null,
    sale_total_amount: null,
    sale_total_disamount: null,
    Cus_id: null,
    customer_Name: null,
    Max_id: null,
    Order_com: null,
    Company_Name: null,
    Order_plant: null,
    Plant_name_th: null,
    Status: null,
    order_dt: [],
    discount_dt: [],
    Total_service: null,
  };

  headerName: string = '';

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController,
    public navCtrl: NavController,
    public alertCtrl: AlertController
  ) {
    this.menuCtrl.enable(true);

    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;

          // console.log(this.userData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    // this.router.events.subscribe((event) => {
    //   if (event instanceof NavigationStart) {
    //     // Show loading indicator
    //   }

    //   if (event instanceof NavigationEnd) {
    //     // Hide loading indicator
    //     this.loadOrder();
    //     this.showPriceDetail = false;
    //   }

    //   if (event instanceof NavigationError) {
    //     // Hide loading indicator
    //     // Present error to user
    //   }
    // });
  }

  ngOnInit() {
    this.loadOrder();
  }

  ionViewWillEnter() {
    // this.loadOrder();
    // this.showPriceDetail = false;
    this.storageService
      .getObject('selectPland')
      .then((result) => {
        console.log(result);
        if (result) {
          this.headerName = 'ชื่อร้าน : ' + result.name2;
        } else {
          this.headerName = '';
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  ngOnChanges() {}

  loadOrder() {
    this.storageService
      .getObject('selectOrder')
      .then(async (result) => {
        if (result != null) {
          this.selectOrder = result;

          const loading = await this.loadingctrl.create({
            message: 'Please wait...',
            duration: 0,
          });
          await loading.present();
          this.apiservice.OrderingByOrderId(this.selectOrder).subscribe(
            (res) => {
              const data = JSON.parse(res);
              // console.log(data);
              if (data.response.responseCode === '0000') {
                this.dataOrders = data.data[0];
                this.storageService.setObject('dataOrders', this.dataOrders);
                console.log(this.dataOrders);
              } else {
                this.presentToast(data.response.responseMsg);
              }
              loading.dismiss();
            },
            (error) => {
              loading.dismiss();
              console.log(error);
            }
          );
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  scanQrcode(data) {
    // console.log(data);
    this.storageService.setObject('ordersQr', data);
    setTimeout(() => {
      this.router.navigateByUrl('qrscan');
    }, 500);
  }

  gotoPayment() {
    this.router.navigateByUrl('order-payment');
    // let checkScanQr = false;
    // this.dataOrders.order_dt.forEach((element) => {
    //   if (element.Mat_code.substring(0, 2) === '20') {
    //     checkScanQr = true;
    //   }
    // });

    // if (checkScanQr === false) {
    //   this.router.navigateByUrl('order-payment');
    // } else {
    //   this.storageService
    //     .getObject('qrscan')
    //     .then((result) => {
    //       if (result != null) {
    //         this.router.navigateByUrl('order-payment');
    //       } else {
    //         this.presentToast('กรุณา Scan ถังแก๊สก่อน!');
    //       }
    //     })
    //     .catch((e) => {
    //       console.log('error: ', e);
    //     });
    // }
  }

  async changeSendStatus(status, txtMessage) {
    const alert = await this.alertCtrl.create({
      header: 'ยืนยัน',
      message: txtMessage,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ไม่ใช่',
          handler: () => {},
        },
        {
          text: 'ใช่',
          handler: () => {
            this.changeStatus(status);
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }

  async changeStatus(status) {
    const loading = await this.loadingctrl.create({
      message: 'Please wait...',
      duration: 0,
    });
    await loading.present();

    this.apiservice.UpdateStatusOrderingOnline(this.dataOrders.Order_no, status).subscribe(
      (res) => {
        const data = JSON.parse(res);
        // console.log(data);
        if (data.response.responseCode === '0000') {
          this.presentToast(data.response.responseMsg);
          setTimeout(() => {
            if (status == 2) {
              this.gotoPayment();
            } else {
              this.router.navigateByUrl('/tabs/history');
            }
          }, 100);
        } else {
          this.presentToast(data.response.responseMsg);
        }

        this.loadOrder();
        loading.dismiss();
      },
      (error) => {
        loading.dismiss();
        console.log(error);
      }
    );
  }

  async changeSendStatusDelivery(status) {
    let checkScanQr = false;
    this.dataOrders.order_dt.forEach((element) => {
      if (element.Mat_code.substring(0, 2) === '20') {
        checkScanQr = true;
      }
    });

    if (checkScanQr === false) {
      this.changeStatus(status);
    } else {
      // this.storageService
      //   .getObject('qrscan')
      //   .then((result) => {
      //     let checkScanQrNew = false;

      //     result.new.forEach((element) => {
      //       if (element.data.qr_code || element.data.qr_code === '') {
      //         checkScanQrNew = true;
      //       }
      //     });

      //     if (checkScanQrNew) {
      //       this.changeStatus(status);
      //     } else {
      //       this.presentToast('กรุณา Scan ถังแก๊สก่อน!');
      //     }
      //   })
      //   .catch((e) => {
      //     console.log('error: ', e);
      //   });

      this.storageService
        .getObject('qrscan')
        .then((result) => {
          let checkScanQrNew = false;
          if (result != null) {
            for (const [key, value] of Object.entries(result)) {
              for (const keyNew in result[key].new) {
                // if (result[key].new.serial_number !== '') {
                //   this.qrscan.push(result[key].new[keyNew].data);
                // }

                if (result[key].new[keyNew].data.qr_code || result[key].new[keyNew].data.qr_code === '') {
                  checkScanQrNew = true;
                }
              }
            }
          }

          if (checkScanQrNew) {
            this.changeStatus(status);
          } else {
            this.presentToast('กรุณา Scan ถังแก๊สก่อน!');
          }
        })
        .catch((e) => {
          console.log('error: ', e);
        });
    }
  }

  gotoOrderDirectionMap() {
    this.router.navigateByUrl('order-direction-map');
  }

  convertStatusText(status) {
    let statusText = '';
    switch (status) {
      case 'N':
        statusText = 'รอยืนยันรายการสั่งซื้อ';
        break;
      case 'W':
        statusText = 'อยู่ระหว่างการจัดส่ง';
        break;
      case 'D':
        statusText = 'จัดส่งเรียบร้อย';
        break;
      case 'S':
        statusText = 'ชำระเงินเรียบร้อย';
        break;
      case 'C':
        statusText = 'ยกเลิก';
        break;
    }
    return statusText;
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }

  // print() {}
}
