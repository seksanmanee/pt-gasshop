import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})
export class ForgotpasswordPage implements OnInit {
  public loginForm: FormGroup;
  public submitAttempt = false;
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiService: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService
  ) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'), Validators.required])],
    });
  }

  ngOnInit() {}

  async forgotPassword() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log('Error!');
    } else {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });
      await loading.present();
      console.log(this.loginForm.value);
    }
  }
  goBack() {
    this.router.navigateByUrl('/login');
  }
  goTerm() {
    this.router.navigateByUrl('/termcondition');
  }
  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'Done',
          handler: () => {
            if (state === 1) {
              this.router.navigateByUrl('/welcome');
            }
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }
}
