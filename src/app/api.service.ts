import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from './storage.service';
export class Users {
  token: string;
  username: string;
  userRole: string;
  singlerole: string;
  account_id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
  password: string;
  loginError: string;
  contentType: string;
  serializerSettings: string;
  value: string;
}

export class orderList {
  response: {
    responseCode: string;
    responseMsg: string;
  };
  data: {
    Order_no: string;
    Order_date: string;
    Order_time: string;
    Cus_id: string;
    customer_Name: string;
    Max_id: string;
    Phone: string;
    Email: string;
    Ship_to_desc: string;
    Latitude: number;
    Longitude: number;
    Order_qty: number;
    Total_amount: number;
    Total_discount: number;
    Status_code: string;
    Status_desc: string;
  };
}

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  userData: any;
  httpHeader = {
    headers: new HttpHeaders({
      Accept: 'application/json',
      'Content-type': 'application/json; charset=utf-8',
    }),
  };

  // apiUrl = 'https://asv-centralportalgateway-dev.azurewebsites.net/api/gasphase-I/';
  // apiMobileUrl = 'https://asv-centralportalgateway-dev.azurewebsites.net/api/mobilephase-I/';
  // apiUrlIII = 'https://asv-centralportalgateway-dev.azurewebsites.net/api/gasphase-III/';

  apiUrl = 'https://asv-centralportalgateway-prod.azurewebsites.net/api/gasphase-I/';
  apiMobileUrl = 'https://asv-centralportalgateway-prod.azurewebsites.net/api/mobilephase-I/';
  apiUrlIII = 'https://asv-centralportalgateway-prod.azurewebsites.net/api/gasphase-III/';

  constructor(private http: HttpClient, public storageService: StorageService) {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          this.httpHeader = {
            headers: new HttpHeaders({
              Accept: 'application/json',
              'Content-type': 'application/json; charset=utf-8',
              Authorization: 'Bearer ' + this.userData.token,
            }),
          };
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
  UsersLogin(users): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'Account/Login', users, this.httpHeader).pipe(
      tap((_) => console.log('users fetched!')),
      catchError(this.handleError<Users>('User Login.'))
    );
  }
  addUsers(users: Users): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'Account/CreateAccount', users, this.httpHeader).pipe(catchError(this.handleError<Users>('Add users')));
  }
  checkemailexits(user): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'chkemail/format/json', user, this.httpHeader).pipe(
      tap((_) => console.log('Email fetched!')),
      catchError(this.handleError<Users>('Email Exits.'))
    );
  }
  checkusernameexits(user): Observable<any> {
    return this.http.post<Users>(this.apiUrl + 'chkuser/format/json', user, this.httpHeader).pipe(
      tap((_) => console.log('Username fetched!')),
      catchError(this.handleError<Users>('Username Exits.'))
    );
  }

  GetCompanyByUser(): Observable<any> {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.get<orderList>(this.apiUrl + `Common/GetCompanyByUser/${userdetail.user_id},M033`, this.httpHeader).pipe(
      tap((_) => console.log('Order List fetched!')),
      catchError(this.handleError<orderList>('Order List.'))
    );
  }

  OrderList(plant): Observable<any> {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.get<orderList>(this.apiMobileUrl + `OrderingOnline/GetOrderingHistoryByPlant/${userdetail.com_code},${plant},02`, this.httpHeader).pipe(
      tap((_) => console.log('Order List fetched!')),
      catchError(this.handleError<orderList>('Order List.'))
    );
  }

  OrderingByOrderId(selectOrder): Observable<any> {
    return this.http.get<any>(this.apiMobileUrl + `OrderingOnline/GetOrderingByOrderId/${selectOrder.Order_no},02`, this.httpHeader).pipe(
      tap((_) => console.log('Order List fetched!')),
      catchError(this.handleError<Users>('Order List.'))
    );
  }

  UpdateStatusOrderingOnline(orderId, status): Observable<any> {
    return this.http.put<any>(this.apiMobileUrl + `OrderingOnline/UpdateStatusOrderingOnline/${orderId},${this.userData?.userdetail?.user_id},1,02,${status}`, {}, this.httpHeader).pipe(
      tap((_) => console.log('UpdateStatusOrderingOnline fetched!')),
      catchError(this.handleError<Users>('UpdateStatusOrderingOnline List.'))
    );
  }

  PostSaleOrder(param) {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.post<orderList>(this.apiUrl + `SaleOrderOnline/PostSaleOrder`, param, this.httpHeader).toPromise();
    // return this.http.get(this.apiUrl + `Material/GetMaterialForDepositSale`, this.httpHeader).toPromise();
  }

  GetMaterialFromScan(qrCode) {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.get(this.apiMobileUrl + `Material/GetMaterialFromScan/${qrCode}`, this.httpHeader).toPromise();
  }

  GetPlantAll(): Observable<any> {
    return this.http.get<any>(this.apiMobileUrl + 'Plant/GetPlantAll', this.httpHeader).pipe(
      tap((_) => console.log('GetPlantAll fetched!')),
      catchError(this.handleError<any>('GetPlantAll Exits.'))
    );
  }

  GetMaterialForDepositSale() {
    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.get(this.apiUrl + `Material/GetMaterialForDepositSale`, this.httpHeader).toPromise();
  }

  GetMaterialGroupByID(grpId) {
    return this.http.get(this.apiMobileUrl + 'MaterialGroup/GetMaterialGroupByID/' + grpId, this.httpHeader).toPromise();
  }

  GetMasterConfig(com_code) {
    // return this.http.get<any>(this.apiUrIII + `MasterData/GetMasterConfig/1018`, this.httpHeader).toPromise();

    const userdetail = JSON.parse(localStorage.getItem('userdetail'));
    const token = localStorage.getItem('token');
    this.httpHeader = {
      headers: new HttpHeaders({
        Accept: 'application/json',
        'Content-type': 'application/json; charset=utf-8',
        Authorization: 'Bearer ' + token,
      }),
    };
    return this.http.get<any>(this.apiUrlIII + `MasterConfig/GetMasterConfig/${com_code}`, this.httpHeader).toPromise();
  }

  GetScoreMaxCard(cid, mid) {
    return this.http.get<any>(this.apiMobileUrl + `Account/GetProfileFromMaxCard/${cid},${mid},0`, this.httpHeader).toPromise();
  }

  GetMIDCustomer() {
    return this.http.get<any>(this.apiMobileUrl + `MasterData/GetMIDCustomer`, this.httpHeader).toPromise();
  }
}
