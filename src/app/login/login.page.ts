import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public submitAttempt: boolean = false;
  constructor(
    private router: Router,
    public formBuilder: FormBuilder,
    public apiservice: ApiService,
    public alertCtrl: AlertController,
    public loadingctrl: LoadingController,
    public storageService: StorageService,
    public toastController: ToastController
  ) {
    this.loginForm = formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {}

  async goLogin() {
    this.submitAttempt = true;
    if (!this.loginForm.valid) {
      console.log('Error!');
      this.presentToast();
    } else {
      const loading = await this.loadingctrl.create({
        message: 'Please wait...',
        duration: 0,
      });
      await loading.present();
      // console.log(this.loginForm.value);
      this.apiservice.UsersLogin(this.loginForm.value).subscribe(
        (response) => {
          // console.log(response);
          let title: string = '';
          let message: string = '';
          let state: number = 0;
          if (response === undefined) {
            title = 'แจ้งเตือน';
            message = 'ไม่สามารถเข้าสู่ระบบได้ กรุณาตรวจสอบ Username / Password';
            state = 0;
            this.presentAlertConfirm(title, message, state);
            loading.dismiss();
          } else {
            loading.dismiss();
            // console.log(response);
            // delete response.singlerole;
            this.storageService.setObject('logined', response);
            localStorage.setItem('token', response.token);
            localStorage.setItem('userdetail', JSON.stringify(response.userdetail));
            setTimeout(() => {
              this.router.navigateByUrl('/tabs');
            }, 500);
          }
        },
        (error) => {
          loading.dismiss();
          console.log(error);
        }
      );
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'กรุณากรอก Username และ Password.',
      duration: 2000,
    });
    toast.present();
  }

  // goBack() {
  //   this.router.navigateByUrl('/tabs');
  // }
  goRegister() {
    this.router.navigateByUrl('/register');
  }
  goForgotpassword() {
    this.router.navigateByUrl('/forgotpassword');
  }
  async presentAlertConfirm(title: string, message: string, state: number) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: message,
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'Done',
          handler: (e) => {},
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    await alert.present();
  }
}
