import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrintSlipPage } from './print-slip.page';

const routes: Routes = [
  {
    path: '',
    component: PrintSlipPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrintSlipPageRoutingModule {}
