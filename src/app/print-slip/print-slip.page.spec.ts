import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrintSlipPage } from './print-slip.page';

describe('PrintSlipPage', () => {
  let component: PrintSlipPage;
  let fixture: ComponentFixture<PrintSlipPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintSlipPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrintSlipPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
