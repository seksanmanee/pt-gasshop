import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrintSlipPageRoutingModule } from './print-slip-routing.module';

import { PrintSlipPage } from './print-slip.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrintSlipPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PrintSlipPage]
})
export class PrintSlipPageModule {}
