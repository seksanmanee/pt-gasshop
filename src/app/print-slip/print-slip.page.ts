import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, NavController, LoadingController, ToastController, AlertController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { StorageService } from '../storage.service';
import { PrintService } from '../services/print.service';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { Plugins, FilesystemDirectory } from '@capacitor/core';
const { Filesystem, share } = Plugins;
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { Platform } from '@ionic/angular';

import * as moment from 'moment';

// import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';

@Component({
  selector: 'app-print-slip',
  templateUrl: './print-slip.page.html',
  styleUrls: ['./print-slip.page.scss'],
})
export class PrintSlipPage implements OnInit {
  pdfObj = null;
  selectOrder: any;
  userData: any;
  dataOrders = {
    Order_no: null,
    Order_date: null,
    Status_desc: null,
    Order_qty: null,
    Total_delivery: null,
    Total_deposit: null,
    Total_discount: null,
    Total_amount: null,
    Ship_to_desc: null,
    Latitude: null,
    Longitude: null,
    sale_total_amount: null,
    sale_total_disamount: null,
    Cus_id: null,
    customer_Name: null,
    Max_id: null,
    Order_com: null,
    Company_Name: null,
    Order_plant: null,
    Plant_name_th: null,
    Status: null,
    order_dt: [],
    Business_place: '',
    Sale_no: '',
  };

  paymentType: any = {
    type: '',
    cash: null,
  };

  bluetoothList: any = [];
  selectedPrinter: any;

  sumPrice = 0;

  loading: any;

  sCore: any;

  constructor(
    private router: Router,
    private menuCtrl: MenuController,
    private nav: NavController,
    public storageService: StorageService,
    public apiservice: ApiService,
    public loadingctrl: LoadingController,
    public toastController: ToastController,
    public navCtrl: NavController,
    private print: PrintService, // private bluetoothSerial: BluetoothSerial

    public loadingController: LoadingController,

    private fileOpener: FileOpener,
    private plt: Platform,
    public alertCtrl: AlertController
  ) {
    // this.storageService
    //   .getObject('dataOrders')
    //   .then((result) => {
    //     if (result != null) {
    //       this.dataOrders = result;
    //       console.log(this.dataOrders);
    //       Object.keys(this.dataOrders.order_dt).forEach((key, index) => {
    //         // key: the name of the object key
    //         // index: the ordinal position of the key within the object
    //         if (this.dataOrders.order_dt[key].Mat_code.substring(0, 2) !== '50') {
    //           const servicePrice = this.dataOrders.order_dt[key].Service_price * this.dataOrders.order_dt[key].Order_qty;
    //           this.sumPrice += servicePrice + this.dataOrders.order_dt[key].Total_price;
    //         }
    //       });
    //     }
    //   })
    //   .catch((e) => {
    //     console.log('error: ', e);
    //   });
  }

  ngOnInit() {
    // this.listPrinter();
  }

  ionViewWillEnter() {
    this.storageService
      .getObject('logined')
      .then((result) => {
        if (result != null) {
          this.userData = result;
          // console.log(this.userData);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.storageService
      .getObject('paymentType')
      .then((result) => {
        if (result != null) {
          this.paymentType = result;
          // console.log(this.paymentType);
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });

    this.loadOrder();
  }

  loadOrder() {
    this.storageService
      .getObject('selectOrder')
      .then(async (result) => {
        if (result != null) {
          this.selectOrder = result;

          const loading = await this.loadingctrl.create({
            message: 'Please wait...',
            duration: 0,
          });
          await loading.present();
          this.apiservice.OrderingByOrderId(this.selectOrder).subscribe(
            (res) => {
              const data = JSON.parse(res);
              // console.log(data);
              if (data.response.responseCode === '0000') {
                this.dataOrders = data.data[0];
                this.storageService.setObject('dataOrders', this.dataOrders);

                Object.keys(this.dataOrders.order_dt).forEach((key, index) => {
                  // key: the name of the object key
                  // index: the ordinal position of the key within the object
                  if (this.dataOrders.order_dt[key].Mat_code.substring(0, 2) !== '50') {
                    const servicePrice = this.dataOrders.order_dt[key].Order_type == 'เปลี่ยนถัง' ? 0 : this.dataOrders.order_dt[key].Service_price * this.dataOrders.order_dt[key].Order_qty;
                    this.sumPrice += servicePrice + this.dataOrders.order_dt[key].Total_price;
                  }
                });

                this.GetScore();
                // console.log(this.dataOrders);
              } else {
                this.presentToast(data.response.responseMsg);
              }
              loading.dismiss();
            },
            (error) => {
              loading.dismiss();
              console.log(error);
            }
          );
        }
      })
      .catch((e) => {
        console.log('error: ', e);
      });
  }

  async GetScore() {
    this.apiservice.GetMIDCustomer().then((res) => {
      const midCus = JSON.parse(res);
      if (midCus.response.responseCode === '0000') {
        console.log(this.dataOrders);
        const cid = this.dataOrders?.Max_id;
        const mid = midCus.data[0].Crm_mid;

        // console.log(cid, mid);
        this.apiservice
          .GetScoreMaxCard(cid, mid)
          .then((resp) => {
            const maxCardData = JSON.parse(resp);
            if (maxCardData.response.responseCode === '0000') {
              this.sCore = maxCardData.data.NormalPoint;
            } else {
              this.sCore = 0;
            }
          })
          .catch((e) => {
            console.error(e);
            this.sCore = 0;
          });
      }
    });
  }

  gotoHome() {
    this.router.navigateByUrl('/tabs/home');
  }

  // //This will list all of your bluetooth devices
  // listPrinter() {
  //   this.print.searchBluetoothPrinter().then((resp) => {
  //     //List of bluetooth device list
  //     this.bluetoothList = resp;
  //     console.log(this.bluetoothList);
  //   });
  // }
  // //This will store selected bluetooth device mac address
  // selectPrinter(macAddress) {
  //   //Selected printer macAddress stored here
  //   this.selectedPrinter = macAddress;
  // }

  async presentLoading(msg) {
    const loading = await this.loadingController.create({
      message: msg,
    });
    return await loading.present();
  }

  success(e) {
    console.log(e);
  }

  failure(e) {
    console.log(e);
  }

  pdfDownload() {
    console.log('pdfDownload');
    pdfMake.fonts = {
      Kanit: {
        normal: 'Kanit-Regular.ttf',
        bold: 'Kanit-Bold.ttf',
        italics: 'Kanit-Italic.ttf',
        bolditalics: 'Kanit-ExtraBoldItalic.ttf',
      },
    };

    let billbody = [
      [
        {
          width: '50%',
          text: '',
          alignment: 'right',
        },
        {
          width: '50%',
          text: '',
          alignment: 'right',
        },
      ],
      // ['รหัสสินค้า\nPrd code', 'รายการสินค้า\nDescription', 'จำนวน\nQty', 'จำนวนเงิน\nAmount'],
      // ['Prd code', 'Description', 'Qty', 'Amount'],
    ];

    let billDeposit = [
      ['รหัสสินค้า\nPrd code', 'รายการสินค้า\nDescription', 'จำนวน\nQty', 'จำนวนเงิน\nAmount'],
      // ['Prd code', 'Description', 'Qty', 'Amount'],
    ];

    this.dataOrders.order_dt.forEach((data) => {
      if (data.Mat_code.substring(0, 2) !== '50') {
        billbody.push([
          {
            width: '70%',
            text: data.Mat_desc + ' x ' + data.Order_qty,
            alignment: 'left',
          },
          {
            width: '30%',
            text: data.Total_price.toFixed(2),
            alignment: 'right',
          },
        ]);

        billbody.push([
          {
            width: '70%',
            text: 'ค่าบริการบำรุงรักษาถัง' + data.Mat_desc.replace('แก๊สถัง', '') + ' x ' + data.Order_qty,
            alignment: 'left',
          },
          {
            width: '30%',
            text: (data.Order_type == 'เปลี่ยนถัง' ? 0 : data.Service_price * data.Order_qty).toFixed(2),
            alignment: 'right',
          },
        ]);
      }
      // billbody.push([data.Mat_code, data.Mat_desc, data.Order_qty.toFixed(2), (data.Total_price - data.Delivery_price).toFixed(2)]);
      // billbody.push([data.Mat_code, 'ค่าขนส่ง', data.Delivery_price.toFixed(2), data.Delivery_price.toFixed(2)]);
      // billbody.push([data.Mat_code, 'ค่าบริการ', data.Order_qty.toFixed(2), '-']);
      // billbody.push([data.Mat_code, 'ส่วนลด', data.Order_qty.toFixed(2), data.Discount.toFixed(2)]);

      // if (data.Deposit > 0) {
      //   billDeposit.push([data.Mat_code, data.Mat_desc, data.Order_qty.toFixed(2), data.Deposit.toFixed(2)]);
      // }
    });

    let logo = '';
    let companyAddress = [];
    if (this.dataOrders.Order_com == '1018') {
      logo =
        'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAEXAWUDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiimSzRwRNLNIkcajLO5wB+NAD6QkKCSQAOSTXE6z8RLW33RaXF9ok6ea4IQfQdT+lcs58T+KDkrdXETHgAbIv6LWMqyWkdThqY+EXy01zPyPRL7xfodgSr3qyuP4YRv/UcfrWDcfEu0UkW2nzSehkcJ/LNZNr8ONTlGbm5t4PYEuf8AD9aun4f6dbj/AEnXVUjrlVT+bVm51d9jHnx9X4I2/rzIX+Jl4SdmnQL6bnJ/woT4mXYI36dA3rtcj/GpV8D6I+dniBDj0ZD/AFqKX4cPIpax1aCb0DpgfmCf5VKlVez/ACInTzGGrv8AgaVr8SrGRgLqxnhz3Rg4H8q6PTvEek6qQtrexmQ9I2+VvyPX8K8r1HwprOmBnms2eIdZIvnX9OR+NYvQ5FHt5xfvIxWPxFJ2qL9D6CoryPRPG2paUyxzubu1HGyQ/Mo9m/oa9M0jWrLWrXz7OXdj76NwyH3FdEKsZ7Hp4fF06+i0fY0KKKK0OoKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooArX1/a6batc3k6QwrwWb19B6msaz8caDe3It0uzG7HCmVCob8f8a888a+IDrermOF82dsSkWDwx7t+P8q5muOeJalaOx9HhskhKkpVW1J/gfRRzg469s155PoniXxVfv/aT/Y7SNyFU/dGDj5V/i+prX8A68dU0j7HO+bq0AXJPLJ2P4dPy9a1PEt9e6fpfnWYX7213IyUB7itKk4un7R7I+fxOXSnX+rzfX0TMyLQvDfhiJZLoLNPjIab52J9l6D/PNU77xtO+UsIFiXoHk5b8ug/WuWllknlaWV2eRjksxyTTK8arj6ktKfuo9/CZPh6EdVd/h/XqW7rVL+9yLi7lkU/wluPy6VUrT0/QNR1IBoYCsZ/5aSfKv/1/wro7XwLEADd3jse6xKAPzOf5VlDDV63vW+bOqpi8PQ926XkjiackjxOHjdkYdCpwa9EXwfpC9YpW+sh/pUcvgvS3U7DPGexD5x+Yrf8As2utdDD+1cO9Hf7jmbDxXqdmQJJftMf92Xk/n1rQnsNC8XqzQj7FqRGeB94+46N/Oo77wTdwqXtJkuAP4CNrf4GubdJrWcq6vFKh6HgqaFWxGHfLVV15/ozKtgsFj4NRtf8AroZeraReaLeG2vI9rdVYcq49QaZp2pXWlXiXVnKY5F/Jh6EdxXf2l5beKbD+yNWwLgDME46lv8f515/qOnz6Xfy2dyu2SM49iOxHsa7FKMkpweh8FmOX1cBVs9ujPYfD2v2+v2HnR4SZOJYs8qf8D2rXrw7RNXn0TU47uEkgHEidnXuK9qtLuG+s4rq3bdFKoZTXbRqc613PQwWK9vG0viRPRRRWx2hRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVyHj7xB/Zel/Ybd8XV2CMjqkfc/j0/P0rqLy7hsLKa7uG2xRKWY/5714VrOqTazqs99Pw0h+Veyr2H5Vz4ipyxst2etlOD9vV55fDH8yhRRWhqejXmkJatdx7BcxeYnt7H36fnXBZ7n1rnFNRb1Y/QNXk0PWYL1MlVO2VR/Eh6j+v1Ar3H/R9RseCJbeePgjoykV8916b8ONe862fR52/eRAvASeq91/A8/j7V04eevI9meLnOFcoKvDeO/p/wAAyrnRrqHWH06ONpJN3yYH3l7Guv0bwlbWQWa9Cz3HXafuL+Hf8a6LYnmGTau8jbuxzj0rl/F+tPaxrYWzlZZBmVlPKr6fj/L61j9Vo4VOrPXseb9cr4txow07nSi4gMnlCaPzB/AGGfyqWvHMkHIPPrXoHhTWm1C1a1uHLXMIyGJ5dfX6j/Crw2PVafJJWIxeWyoQ54u66nR0UUV6J5YVnato1rq8G2ZdsoHySgcr/iPatGipnCM48sldFQnKEuaLszya+srnSb4wy5SRDuVlPX0INaHiBE8QeG01VQBfWOEuAB95D0P9fzrsfEekDVdObYo+0xfNEfX1X8a4jQbhYtRNrP8A8e92pt5lPo3H868bkeFrcn2Zf1+B62KhHM8FJNe+v6/E4yvQvhzq5Im0mVun72HP/jw/kfzrg7u3a0vJ7Z/vQyNGfqDirOiX50zWrS8zhY5Bv/3Tw36E11U5ckrn5/hqro1lL7z3OigEEZByDRXon1AUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUVj+JtbTQdFlusgzt8kKnu5/oOtJtJXZdOnKpNQjuziviN4g8+4XRrd/3cR3XBB6t2X8Ov1+lcDTpZHmleWRi8jsWZj1JPU0+2t5bu6itoELyysFRR3Jry5zc5XPusNQjhqKgum50vgbw/8A2xq4uJ0zaWpDuD0duy/1P/169E8WaGNd0OSFAPtMX7yA/wC0O34jj8qt6DpEWh6RDZR4LKN0jj+Nz1P+ewFaVd9Okow5X1PlMZj51cT7WD0jt/XmfOrKVYqwIYHBBHSrGn302mahBe25xLC+4e/qPxHFdT8QtC/s/VRqEK4t7sktj+GTv+fX8642uCUXCVj6yhVhiaKmtn/TR7uuu2j6AurqcwvHuVc87um3654rzW6uZby6kuJm3SSNuY1k6Zqc4thpryn7PvMiL2DYxWhXHj68qklHojzsNgY4WUvP8gqzYXsun3sV1CfmQ5x6juKiMMogE5jbyixQPjjPXFR1wpuLTR0tRkmnqj160uor20iuYTmORcj29qmrhPB2r/Z7k6fM37uY5jJ7N6fj/Ou7r6fDV1WpqXXqfJYvDuhVcOnQKKKK6DmCvOPFVibDW2lj+VJ/3qkdj3/Xn8a9HrmPG1qJdKiuAPmhkx+Df/XArix9Pnot9Vqd+W1fZ10uj0POvEhEmtSXC/8ALdElI92UFv1zWTV/VW3Swc5Iiwef9pqoVzRlzRUu58ZmFNU8XUgukn+Z7d4buTeeG9PmJyTCqk+pXg/qK1K5fwBIX8LRqTny5XUD05z/AFrqK9SDvFM97Dy5qUX5IKKKKo2CiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAQkAEkgAdSa8W8Y6+dd1pjG2bSDKQjsfVvx/liu2+IPiD+z9OGm274ubofOQeUj7/n0/OvKK4sTU+wj6XJcHZfWJ9dv8wr0f4ceH9qtrVwnJyluCO3Qt/T864zw9o0uu6xDZplUPzSuP4UHU/0+pr3OCCK2t44IUCRRqFRR0AHSlhqd3zM0znGezh7CO739P+CSUUUV3Hyxn63pUWtaRPYy4HmL8jY+6w6GvCbm3ltLmW3nQpLExR1PYivoVmVEZmICqMknsK8X8XTLqOrzahEgVHIXgdQOAT7muHGOCcb7s+hyKrPmlT+z+v8AwTnlJVgwOCDkGun0tJNVkhigGZZG249D3/DvXL10/gTVYdM8QxrcAeXcDyg5/gY9D/T8a4pUY1Wkz3MVzKlKUVdpHp8mhW76D/ZYGFVflfHO/wDvfnXmlxBJbXEkEq7ZI2KsPevYK5DxnpG+MalCvzL8swHcdj+HT8q3zDCp01OC+H8v+AfM5bi3Go4Tfxfn/wAE4tWKsGUkMDkEdq9P0DVRq2mrKxHnp8so9/X8a8vrU0DVTpOpLKxPkP8ALKPb1/CvPwWI9jU12e56ePwvt6Wm62PUKKRWDKGUgqRkEd6WvpD5UKy/EcQm8P3ikdE3fkQf6VqVS1gA6JfZH/LvJ/6CazrK9OS8ma0HarF+aPE75s3RHoAKrVJO2+4kb/aqOvMpq0Ej5XH1FVxVSa6yf5nqXw4J/wCEcnznAumx/wB8rXYVyvw9Qr4XBP8AHO5HH0H9K6qvTpfAj3cIrUI+gUUUVodAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFVtQvoNMsJry5bbFCu5vf0A9yeKs15d8RPEH2u8GkW75htzmYg/ef0/D+f0rOrPkjc68FhXiayh06+hyOqalPq2pT31wf3krZx2UdgPoKp0V13gLw//AGrqv224TNpaENgjh37D8Op/D1rzoxc5W7n2dWpDDUXJ6JL+kdv4J8P/ANiaOJJlxeXOHkyOVHZfw7+5rpqKK9OMVFWR8NWrSrVHUnuwooqlq+pw6Ppc99P92Jchc/ebsPxNNtJXZEYuUlGO7MHxjrAht/7OgcebJzLg/dX0/H+X1rhJEWSNkYZVhg1nJqs93qs1zdPue5fLH0Pb8O1adfOY2cpVeZ/I+vw+F+q01Dr19TnJomhlaNuoP51HXe+I/CD2/hmHUEUm7h+a4X/YP/xP9TXBV2csopc+524fEQrx5oPbQ9p8Ga7/AG3oaGVs3VviOb1Po34j9Qa6CSNJY2jkUMjgqwPcGvEvCeuHQtcjnc/6PJ+7nH+ye/4HmvblYMoZSCpGQR3r0aE+eFmfJ5nhPq1e8fheqPLda0x9J1KS3OTGfmjb1U/5xWdXpfiTSRqmmny1zcQ5aP1Pqv4/4V5ocg4NeDjMP7GpZbPY9nA4n29K73W53fg7V/tFsdPmb95CMxk909Pwrqa8itLqWyu4rmE4kjbI9/avVLC9i1GxiuofuyDOP7p7ivUy7Ee0h7OW6/I8jM8L7OftI7P8yzWT4muBa+GtQlP/ADyKjnu3A/nWtXBfEjVNkFtpaH5nPnSfQcKPzz+VdtVpQdzxa9b2NNzW6/M86oop8UTzzRxIMu7BVHueK88+V3PY/B9v9m8KWC4wWQyH/gRJ/rW5UVtAttawwJ92JAg+gGKlr04qySPraceSCj2QUUUUywooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoopGZURnchVUZJJ4AoAxPFevLoOjSTqR9pk+SBT/e9foOv5V4izM7s7kszHJJ6k1ueLNebXtaeVGP2WL93AP8AZ9fx6/lWFXm16nPLTY+0yzB/VqPvfE9/8ieztJr+8htLdd0srhFHua910bSodF0qCxh5EY+Zu7Meprj/AIc+H/JgbWbhP3kgKW4PZe7fj0+n1rv66cNT5VzPqeLnOM9rU9jHaP5/8AKKKK6TxQryn4h6/wDb9SGmQNm3tW+cg8NJ3/Lp+ddz4r11dB0WSZSPtMv7uBf9r1+g6/lXiTMWYsxJYnJJ71yYmpZciPoMkwnNL6xLpsJXoPgOwOqzC8nXMdqccjhn7fl1/KuHsLKbUb+CztxmWZwq56D3+gr3XSdMg0fTIbG3HyRrgtjlj3J9zXPRoKpJSlsjtzjFKlS9nH4n+RbdEljaORQyMCrKRwQe1eG+JdFfQtbmtMHySd8LHuh6fl0/CvdK5fxzoX9r6I00SZurXMiY6sv8S/lz9RXZXp88brdHjZVi/YVuWXwy0/yZ45XrPw913+0NKOnTNm4tBhcnlo+35dPyryatDRNVl0XV4L6LJ8tvnUfxKeo/KuOlU5JXPpMfhViaLh1Wq9T3uvP/ABdpH2O9+2QriCc/MB/C/f8APr+dd3bXEV3axXMDB4pVDow7g1HqFlFqNjLayj5XGAf7p7GurFUFXp8vXofJYTEPD1bvbZnkldL4Q1f7He/YpmxDOflyeFft+fT8qwLu1lsruS2mXEkbYP8AjUIJByK+dpVJUaikt0fT1acK9JxezPY2YKpZiAoGST2rw/XtSOra1dXhJ2O+IweyjgfpXrHhvVxqumjef9Ihwsnv6N+P+NbNfSaV4KUXofDY/BTm/ZSdrHz7XReCLD7d4nt2IyluDM34dP1Ir2Cipjh7O9zhp5YoTUnK9vL/AIIUUUV0nqhRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVxfxG1mSx0mOwhJD3mQ7DsgxkfjnH0zXaVyHj3w9cazYw3NmhkuLbd+7HV1OM49xis61+R8p2Ze6axMHV2/q34nkdFTvZ3UTlJLaZGHBVoyCKjeKSMZeNlz/AHhivLsfcKSexNHqN9CoWK8uIwBgBZWHH51J/bGp/wDQRu/+/wC3+NUqKd2J04PdF9Nb1aM5TVL1T7XDj+tO/wCEg1r/AKC9/wD+BL/41nUUcz7k+xp/yr7ixdX95fFTeXc9wU+6ZpC+Ppmq9Fbfhnw7ca/qUcYRhaIwM8uOAPQH1NNJydkE5wowcpaJHZfDnQPJt21m4X95KCkAI6L3b8en0+tbXjDxMfDthH5CK93OSIw3RQOrH8xXQxRJBCkUShI0UKqjoAOgrhfiVpNxdWtrqECM624ZZQvOFOCD9OufqK75RdOlaJ8lSqxxmOUq2z/pI49vGfiFpTJ/acgJ7BVA/LGKP+E08Rf9BOT/AL4X/CsGiuH2k+59V9VofyL7kOd2kdnblmOTxjmm0UVBuek/DbXC8cujTNkoDLASe38S/nz+Jr0KvKPhxptxNrxvwhFvbowLkcFiMYH55r1evRw7bhqfG5vCEcU+Trv6nK+MdI8+3Gowr+8iGJQO6+v4fy+lcLXsbKGUqwBUjBB715jr+ktpOotGAfIf5om9vT6ivLzLD8r9rHrudmVYrmj7GW62Kun6lc6XcGa1cKxXaQRkEVrDxpqoGCLc+5T/AOvXPUV58K9WCtGTSPTqYelUd5xTZ0X/AAmmq/3bf/vg/wCNWbPxxciZReW8TRZ5MYIYfrg1ylFaLGV078xlLA4dq3Ij2GGVJ4UmiYNG6hlI7g0+s7QoJLbQ7SKUESBMkHtnnFaNfSwblFNnytSKjNxWyCiiiqICiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigApjTRIcNIin0LCn15d8R7Yx69BOB8s0A59wSD+mKipPkjc58TWdGnzpXPUCyhdxYBfXPFRu1vIMO0TD3INcxoT/274CFuTulSMw/8CXlf021w9ceJxro2926Z62XYWOMp+0UrHr4t7cjIhiIP+yKRrO2cYe2hYehQGsnwndi50GJc/PCTG38x+hFblddKaqQU11OarGVKo4N7Fb+zrH/AJ87f/v0v+FToiRoEjRUUdAowBTqK0sZuTe7CiiigRQfQ9IkcvJpVi7HqWt0JP6VG3h3RGXB0ix/C3UfyFadFLlXY0VaotpP7zJ/4RjQv+gTaf8AfoUf8IxoX/QJtP8Av0K1qKXJHsV9Yq/zP72RwwxW8KxQRJFGowqIoAH4CpKK57xT4nj0C1VYgkl7J/q426Aepx2olJRV2c85qKc5M6Gobm0t7yExXMKSxnnDCvNP+Fkax/z7WP8A37f/AOKo/wCFkax/z7WP/ft//iqydem1ZnGszop3TZ3B8K6KTn7F/wCRX/xpreEtGJ4tWX6St/jWF4b8Ua9r+oCIW1klvHzNKI3+Ueg+bqa7ilGjQmrqC+49CjmFWrHmjOVvVmH/AMIjo/8Az7v/AN/G/wAantfDelWcwmitQZF5UuxbH4GtWiqWHop3UV9xq8VWas5v7wooorYwCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK474i6ebjRYrxRlrWT5uP4WwD+u2uxqC8tIr6ymtZhmOZCjfjUzjzRaMq9P2tNw7nm/w81cWupSadK2I7kZTPZx/iP5CpfFGnHT9YkZVxDP8AvE/HqPz/AJiuSure40jVJIWJS4tpOGHqDkEfoa9Lt7iDxr4b4KJfQ/eH91//AIlv89K82rS9tScOq2/yI4fzD2FT2VT+v+GMnwhqYs9TNtI2IrnCjJ6N2/w/KvQq8ekjkt5mjkVkkRsEHggivQvDWvrqduLedgLyMc5/5aD1Hv61GXYhL9zL5f5H0eaYVt+3h8/8zfooor1zxAooooAKKKKACiiuS8ReNbbTUaCxZZ7k8bhyq/4/yqKlSMFeQbRcnoluzS8ReJLbQbQkkSXTD93Fn9T7V5BfXtxqN5JdXUhklkOST/L6U27u5725e4uJGkkc5LE1DXDOo5u7PnsbjPbvlh8K/HzCremabc6tfx2dqm6RzyeyjuT7Cm2Nhc6leR2tpEZJXPAHb3PoK9f8OeHbfw/ZbFxJcyYMsuOp9B7CnSpub8jPCYWVeWvwotaNpFvounR2luM45dyOXbuTWhRRXekkrI+kjFRXKtgooopjCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigDifHvh1r23/ALVtUzNCuJlH8SDv9R/L6VwWkatc6LqCXdq3zDhkPR17g17n1GDXmni/wa9o8mo6bFutj80kKDmP1IH93+X0rlrU2nzxPIx2Fkpe3pb9f8zXkXT/ABnZ/a7Blh1FF/eRMeT7H19m/wAjlnjubC62uJIZ4z9CDXP29xNazrNbyvFKpyrocEV1MPjGG/hS316yFwFGBcw4WQfh0P6fSuCvQjVfMtJfgz1sr4jVOKpYnbudHpPjNdqw6mpyOPOQdfqP8Pyrqra8trxN9tPHKv8AsNnFeYtb6ZcfNp+rQMD/AMs7k+S4/E/KfzqL7JdwvlEYkc7om3D81ohi8RR0qRuv66ntOjgcV71Col/XY9aoryoatqsA5vrtRj+KRv61BN4kvGXD6ncEeiyHn8q3WZJ7QZz1MDClrOrFLzZ6xNPDbpvmlSNf7zsAP1rDv/GGmWanynNw47Jwv4k/0zXl9xqrytn5nb+9I2aoSTSSnLsT7dqHiq09ly/izysRj8DQ0g3Ul5aL79/uOk1vxlfamGiR/LhP8CcD8e5/lXMsxZizEknqTSUVmlrd6s+fxeOrYp+/olslsv67vUK0NI0a81u8FvaR57u7fdQepNb3h/wLeaiUuL8Na2p52kfvH+g7D3P5V6XY2FrplqttZwrFEvYdz6k9zXTToOWr2NcLgJ1PenoijoPh600C1McA3zP/AKyZhy3+A9q16KK7EklZHuwhGEeWKsgoooplBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHH6/4DtdRZrjTytrcnllx+7c/Tsfp+Veealo2oaTL5d7bPH6PjKt9COK9zprokiFHVWU8FWGQawnQjLVaHBXy+nVd46M+f6K9hvfBWhXpJ+yeQx/igbb+nT9KxpvhnaMT5GozIO3mRh/5YrB4ea2POnltdbWZ5vRXf8A/CsW34/tYbfX7Pz+W6povhlAP9bqkj/7kIX+ppewn2M1l+I/l/FHnVOjjeVwkaM7noqjJNesWvgDQ7cgyRzXBH/PWTj8lxW/Z6dZaemy0tYYB38tACfqe9WsNLqzop5XUfxux5fpXgPVr9la5UWUJ6tKPm/Bev54rvNH8JaVo22SOHzrgf8ALaXkg+w6Ct2it4UYxPRo4KlS1Su/MKKKK1OsKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAP/Z';
      companyAddress = [
        'บริษัท แอตลาส เอ็นเนอยี จำกัด.\n',
        'สำนักงานใหญ่ เลขที่ 90 อาคารซีดับเบิ้ลยู ทาวเวอร์ เอ ชั้นที่ 24 \n',
        'ถนน รัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310\n',
        'เลขที่ประจำตัวผู้เสียภาษี 0105554147681',
      ];
    } else {
      logo =
        'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAeAB4AAD/4QBaRXhpZgAATU0AKgAAAAgABQMBAAUAAAABAAAASgMDAAEAAAABAAAAAFEQAAEAAAABAQAAAFERAAQAAAABAAAewlESAAQAAAABAAAewgAAAAAAAYagAACxj//bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAGIAiQMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ooooAKKKKACiiub+Kvxf8NfBDwjLrvirWLXRdLjYR+bMSWlcgkJGigvI5CsdqAthScYBrKviKdGm6taSjGOrbdkl3beiA6SmXFxHaW8k00iRRRKXd3O1UUckk9gPWvzy+P3/AAV18TeKLq4sfh3psXhvT1cqmqX8aXF7OORuWIgxx9jht5OB0yQPlXxx8QvEXxQuVm8T+INa8RTRkmNtQvZbgRZ5IQMxCDOcBQAK/Nsy8TsLTnyZfRdb+83yQ+TalJ/+A2elnZnFPGxXwo/XbxD+1x8LvCr7L74heDYpt20xJq0MkoOCeUViwHB5xiueuf8AgoP8GbSVkbx9pLMpx8kczj81QivySigWHdtULuOTROxSFmH8IzXzOI8TM2VN1I06cbJu1pS28+aF9PJamSxs72sj9pPhL8d/CPx1sLy68I65a65BYOsdw0KuvkswyAQwByQM111eDf8ABNf4Vn4XfsjeHWmhWG+8S79eusHIf7Rgwn2P2dYAQeQQfpXvNfr2RYrE4rL6OJxcVGpOKk0rpK6vazbe2+u5305NxTkFFFFesWFFFFABRRRQAUUUUAFFFU/EPiCz8J+H77VNRuI7TT9Nt5Lu6nf7sMUalnc+wUE/hUykopyk7JAcD+1J+0/oP7K3w1m13WP9LvZsxaZpkcgSfUpuPlUnO1FyC74IUdmYqrflN8cPjr4k/aR8czeIvFF4bqZ8raWi5Frp0RORHEh4UcDJ+8xAJJPNaP7Tv7Q+pftQ/GDUfE1959vp7f6NpVg75WytV+6MdN7ffc92YgfKFA6D9k/9i/xR+1l4gk+x/wDEn8L2Lhb7Wp0JTdxmGBRjzJcHJGQqjliCUV/5z4i4gxfEOMVLCpyo7U6aXxtfbl5dVze7FWej1PLrVJVZcsf6/r8PvPJdPsbnWdUt7CwtbrUNQvG8u3tbaFpprh+yoigsxPoBmvoD4Wf8EwPiz8SLeO5vNP03wnZyLuDavckTMPaGMM6n2fYe/ses/wCCjfxt0n/gkT8IvDfhf4O6bptn8QPGwnMviG/jhvdQs7SHy90jhx8xkkdQilfJGyUhMiviCX/gtn+01Gfm+Jr5bsvh3SyfyFrSnleBy7EexzqpOU7JuFG3Krq9pTlaTfX3VHpvqfuXA30duJOJsrp5vhZ0oU6jlyqcpqTUXZySjCatdNa2ejdrWb/RHQv+CLUrQ+ZqfxG2zNjMdpo2UXGf4nm5zx/CMe/Wpn/4IoWkt/GW+JF39jLgzx/2IolkXPIV/OwpxwCVODzg9K/OVf8Agtt+0wzY/wCFmTc+vhvTP/kWu+/Z7/4L5/Gz4dePre88d6pp/j7wtIVW8s5NMtrO5hjzlnglt0jHmY6CQOpHGATuHo0XwRVkqdXDVEu8p1La7tpVHv1stddD6zGfRQ4sw9GVWMqM5JXUVOpzSfaPNTjG76XaXmfrJ8dPjnr37Mr6JHpPwm8WeNvBMNkYrq78LPDeahpLIVSKMWDMss0ZXkvGzFdpyp4zvfs7/tU+AP2rPC82reA/EljrkNmwjvbdd0V5p0h3AJcQOBJExKsAHUZ2nGQM11Xw98dab8UPAOh+JtHma40jxFp8Gp2MrIY2kgmjWSNip5UlWBweRXzT+39/wTwk+NN8fih8KdQm8C/HbQIxLYazp0otf7cCKALW7P3XDIojDPkYCo+6P5R+3VpV6a9rRtKNvh6/9utfk0790fy/mUcwwc5VKS51Hem1yyVtGovTVfyyTu9OaJ9XUV8kf8Ev/wDgpWP2zNL1Twb4x01vDPxe8GK6a5pbwNCtwsUghknRG+aNkkKpJE3KOw6gjH1vXTh8RTr01UpvT+tzoy3MsPj8PHFYZ3jL5NNaNNdGno0FFFFbncFFFFABRRRQAV8l/wDBXj4xSeC/gVpfhK1k8u78bXhWY44+yW22SUAg5BMjW49CpcV9aV+a/wDwWB8TNqv7TGi6aH3W+j+HopNuD8k0s85b6/IkfT1r4nxCxk6GSVIQ3qNQ/wC3ZP3/APyTm+Zz4qfLA8a/Zc/Zv1P9qf4uWvhmxkktLGJftWrXwUEWVqCAxGeDIxIVF55OSNqsR+u3gPwHo/ww8Haf4f0Gwg0zR9LhEFtbRD5Y16kknlmJJZmYlmYkkkkmvnz/AIJS/BuH4c/syQ69JFt1bxtcvqE7MgVlgRmjt48jqu0NIM8gzsK9h/aZ+NNv+zp+z34y8c3XkMvhfSLi/jjmYqlxMiHyoiR03ybE/wCBVw+H+SwwWWrM8SrVa0eZvflhvGK7aWlK28m+yN8rwFSvVhQoLmnUail3bdkvm2fh5/wWg+O//C9v+Cg/jDyZhNpvgsR+FrImERsn2bJuFJ/ixdyXOCewHYDH1X/wbafAX9z8Rvilcxr87xeFdNkWbJwuy5uwyY4yWs8HP8LCvyw1TWLvW7661G/kkub++lku7mQ8tLK7F3P1LE1+qPxI/YX8dN+wZ8AvAPgz4neCfhpcafp0vijxCuua9Lol7qF9fKJFG2CNmYQeZPFuZuQFGPlr4rhmvLFZxXzeUHPkvJRW95OyS6aJt6vpfV6n+iviVl+DyvhPB8GLFRoRrctF1Gm0oUo8852jq+eUYxfd1NerP0R/an+O1j+zL+zp4y8eX8lssfhnSpruFLhisdxcbdsEJxzmSYxxjHdxX80Xhvw5qXi/XNN0fSbWW/1nWLmKysrWFDJJdXErhUjRepLMQAPevuGD/gjx448feJbT/hNv2jPg7cab5i+dc/8ACVXOq3EK5GSkUwjVmA6AyL9RX25+wT+wj+zz+wxq0PiSL4geGvGPjxLd7ca7qOrWka2YfIk+y26uVh3L8pYs8m0socK7KffznLcw4ixdL2tP2NGF9ZSTerV7JdbJW6eZ+e8H55wv4Z5VivqOKeYYqs4tKFKcY+6pKCk5XikpSk5O7k00lDS59V/s5fDa6+DP7PfgTwffTwXV74V8PWGkXE0GfKlkt7aOJmTIB2lkJGQDgiuzrjv+GiPh/wD9D14O/wDB1bf/ABdbXhP4gaD48jmfQ9b0jWktiFmawvI7gRE5wG2E4zg4z6V+qUFThBUqb0SSWvRH8e46GLqVZ4rEQacm23ZpXbu/xZ+Q3/BYjQ9V/YI/4KYeDPjb4Mhhs5PEaLrAjUeXFdXttiC9hYLj5JoJIRIR8zG4lPU7q/XD4UfEzSvjP8MPD3i7Q5Hm0fxNp0GqWTOu1/KmjWRQw/hYBsEdiCO1fmT/AMHOMqC1+CiFV8xn1xlP8QAGn5x+Y/IV9K/8EKPHN34z/wCCb/hOG83NJoN7f6ZG7dXjW5eRP++VlCjHZBXm4dunjZ01s9fno/vd38ku2v4/w/ivqvFmPyuHwTSqJdpNRcvv5vwR9g0UUV7B+oBRRRQAUUUUAFfmb/wWI0RtH/aX0+9G8Q6t4egIdunmRzToyj/gOw8926+n6ZV8j/8ABXv4J3Hj34KaT4ts42lm8E3EpulGcrZ3ARJXAGclXjhJ9F3tkAHP5/4n5fWxXD1b6v8AFC0vktJfdFyfyMcRFOF30Pob9nawi0r9n7wLawtvht/D1hFG2c7lW2jANfMX/Bcuz8feOP2Q7bwR8P8Awn4l8VXni3V4F1WPStKe9EVjb/vyHKg+WWnW3x/eCuOxr1//AIJ0/FSH4q/sj+FXWdZbzQIf7DvIwMNA9vhEVvcw+S2e4cZ5yK9wr38FGjmeS040ZcsKlONmt0nFaa36aeXqfRcI8QPJc1w2cRpxqujJTUZXs5R1V7a6Ss/VH4Q/sQf8Eefi18bfjXoMnjTwRq3g/wADabfw3Osz65ELWW5hjcO0EMLHzGaQKV3bSi5ySeAfSv8AgqL+xB+0F+2B+2n4r8VaX8NdevvDdmY9H0Npry1Km1t12mRFaUFUlmM0oGAcSjPOa/ZSivIjwDgI4F4GM5pOSk2mrtpNJbWsr3236n7JivpHcR1M7jndKjRUoU5U4RaqOMFOUZTkrVE+eXLFOTduVWSWp/NR8f8A9hz4lfss6Np+oeP/AAZL4bs9Vna2tJLie2czyKu4qqo7NwvJOMDjJ5GeO+FHwa1z45/EXSfCXhLQ5Nc8Ra5MYLKygVFaVgpdiWYhVVUVmZmIVVUkkAV9vf8ABw58e/8AhYn7Xei+BreZnsfh3pCmeNoAuy+vds0mH6sPs62nsCWHrVj/AIN2/gIfHv7V/iHx7cwrJY/D/RzBbSbgGjvr0tGpA7gW8dyDjGN6+tflc+H8PUz9ZThZScE7Ntq+ivK2iWmqWnTqf1zhfEbNsN4ff625xyxr+ydRRjzqF5ytRTUpuTUuam5Wls3Y8R07/gjJ+0dqszRw/Cm6V1Xd+/1LT4FPb7zzAd+nX8q/Xj/glH+xFffsK/stx+HdckspvFOuX8msav8AZSHitpHVES3STapdUjjXOcje8mCQQT9MV80/8FP/APgoNpf7BXwHmvIGt7zx14hSS08OaczA/vcYa6lXr5MO4MR/G2xMrv3L+t5Pwjl+TVJYunKUna3vNO1+1ktXt+C3P4e8TPpBZ7xDkrwmdunToQanLkU020mknzTnfV6JWbdt9D8w/wDgvx+0bb/Gj9tn/hG9Ok83Tfhvp66S7htyveyMZrkr/uhoYiOoeB/av0I/4IK+H5tH/wCCbfhe6kBC6vqep3cecfdW7kg/nCeuPyxX4Sxx6t8QfFqqv2zWte128wNzGW4v7qZ+5PLSPI3U8ktX9NH7L3wUg/Zx/Z18FeBbcwyf8Ivo9vYTSxZ2XE6oPOlGefnlLv2+90HSvTy+88S5vs21/iemvkk1/SP4z8N61bNeIMXnNRWTVvTma5V8oxsd5RRRXvH7oFFFFABRRRQAVDf2EGq2M1rdQw3NrcxtFNDKgeOVGGGVlPBBBIIPBBqailJJqzA+E/8AhBte/wCCWPxxvPEGnWuqa/8ABnxIUi1DYTLJoxLgRl/9pC21XbAkV9hO/a1fa3gbx1o/xL8J2Ou6DqFrquk6lH5tvdW77kkGSCPUMCCpU4KsCCAQRWhqOnW+r6fPaXcEN1a3UbQzQzIHjmRhhlZTwVIJBB4INfPGsfsIXfwv1261z4K+Lrz4e315J5tzpE6fbdDvD7wtzGT/AHhu2jhAgr4jDZXjcinJZbD2uFk2/ZJpTptu79m21Fwe/I3Fp6pu9jnjTdPSO34n0bVPxF4gs/Cfh++1XUrhLTT9Mt5Lu6nfO2GKNSzucdgoJ/CvlX9o/wD4KFeMv2CvAlhrXxg8DaLqNjf3qadBf+EtY3/ap2jeTC2tyqOqhY3JYuQDtHOQa8j1/wD4OKfgD4r0G80vVPAfxO1LTdTt3tbyzutH0yaC6hdSjxSI16VdGUkFSCCCQa+go51QqxbSlFrpKE4tP5x19VddmeViuKsnweIVDHV1B6Np6Oz/AA9D8l/jx8YLz4/fGvxZ441HMdx4s1a41Pyi5ZbdJZCY4lJ52om1APRRX7df8EOv2fZvgZ+wRoV9qFpNZ6147uZvEV1HPEFkjikxHbLkclDbxxyAHoZm4GTXxd41/wCCxX7PXhvURceA/wBk7wXNdW7eZaXuqabpmnS28g5STZBbzHIIBwJAfQjrXgv7T/8AwWU+O37TtrdafN4lXwb4fuuG03wyrWIkXBXDz7muGDAkMvmBGzynTHxfDuQrLsVUx9ap7WpJO3uuKXM7ttvq9nZXS6an694z/TA4ZzvIqeQ5Lh5RhCUW0nfSEXGEE7Jcuqbabd4rTRn6l/8ABQz/AILDeAf2KrG/0DR5bbxp8Slj2xaPazZtdMckjdezLkRlcE+SuZW+QERq4kH4f/tA/tCeLv2ofinqHjLxtq0usa5qJClyNkVtEudkMKDiONcnCjuSTlmZjxYGK+tP+CZP/BK3xN+3f4wttY1SO60H4XafcldS1b7k2olfvW1mCCGcnCtIfkjBY/MwCH6avWqVZLmV30S/r8Xsu1z+FM24gzji3GRwdGNo30gtl/ek+tu7sl0Svr7Z/wAEA/2B7j4lfFD/AIXV4msP+KZ8JyvB4eSeMFdR1LG1pwrA5jtwThgP9cVKtuhcV+ylYnw3+HGhfCDwJpXhjwzpdroug6Jbra2VlbrtjgjXoPUk8ksSWYkkkkk1t17mDw/sadnu935/8DZH9DcK8O0sly+ODp6y3k+8nu/RbLyXe4UUUV1H0gUUUUAFFFFABRRRQAUUUUAfix/wcYfH/wD4Tz9qjw74BtLjzLPwDpPnXcY42X17tlZW9cW6WrA9vNbpzn88q/oo+Lv/AASY/Z/+PHxH1bxd4s8Byat4i1ybz767Ov6nCZ32hQdkdwqLhVUAKAAAOK5+D/giN+y/bOrL8MQShyN3iLVmH4g3Rz9DXhywOIlVlKVtXfd38tOXotPkfg/EfhnnWaZlWx3tadpy0TcrqK0in7j1SSufz716R8Av2Qvid+1JfrD4B8Ea/wCJI92xruC38uxib0e5kKwIeDwzgnBr+gf4ff8ABO/4F/C+ZJtF+E/gSG5iYPHc3GkRXdxGR0KyzB3X8DXsVvbx2lvHDDGkUUShERF2qijgADsB6VUctqt+/JJeWr/S33MeX+DL5k8diNO0F/7dL/5Fn5lfsR/8G8WjeDbuz8QfGzVLfxNexkSJ4a0uR005Dg4FxPhZJsEqdiCNQyEEyoSD+lugaBY+FdCs9L0uytNN03TYEtbS0tYVhgtYUUKkcaKAqoqgAKAAAABVuivSo4anS+Bavd9f68tj9dyTh3L8ppexwNNR7veT9W9X6bLokFFFFdB7QUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAH/9k=';
      companyAddress = [
        'บริษัท แอตลาส เอ็นเนอยี จำกัด.\n',
        'สำนักงานใหญ่ 90 อาคารซีดับเบิ้ลยู ทาวเวอร์ เอ ชั้นที่ 33 \n',
        'ถนนรัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310\n',
        'เลขประจำตัวผู้เสียภาษี 0105535099511',
      ];
    }

    let content = [
      {
        text: moment().format('DD/MM/YYYY HH:mm:ss'),
        alignment: 'right',
      },
      {
        image: logo,
        width: 80,
        alignment: 'center',
      },
      {
        text: 'ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ',
        style: 'header',
        alignment: 'center',
      },
      {
        text: companyAddress,
        // style: 'header',
        alignment: 'center',
      },
      {
        text: 'สาขาที่ : ' + this.dataOrders.Plant_name_th + '(' + this.dataOrders.Order_plant + ')' + ' (' + this.dataOrders.Business_place + ')',
        style: 'header',
        alignment: 'left',
      },
      {
        text: '----------------------------------------------------',
        style: 'item',
        alignment: 'left',
      },
      {
        text: 'เลขที่ใบกำกับภาษี : ' + this.dataOrders.Sale_no,
        style: 'header',
        alignment: 'left',
      },
      {
        text: 'วันที่ : ' + moment(this.dataOrders.Order_date).format('DD/MM/YYYY'),
        style: 'header',
        alignment: 'left',
      },
      {
        text: 'Member : ' + this.dataOrders?.Max_id + ' ' + this.dataOrders?.customer_Name,
        style: 'header',
        alignment: 'left',
      },

      {
        text: ' คะแนนคงเหลือล่าสุด : ' + this.sCore + ' แต้ม',
        style: 'header',
        alignment: 'left',
      },
      // {
      //   text: '----------------------------------------------------',
      //   style: 'item',
      //   alignment: 'center',
      // },
      {
        layout: 'lightHorizontalLines',
        table: {
          headerRows: 1,
          widths: ['*', 60],

          body: billbody,
        },
      },
      {
        text: '----------------------------------------------------',
        style: 'item',
        alignment: 'center',
      },
      {
        columns: [
          {
            width: '50%',
            text: 'ราคาสินค้ารวมภาษีมูลค่าเพิ่ม',
            style: 'summary',
          },
          {
            width: '50%',
            text: this.sumPrice.toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },
      {
        columns: [
          {
            width: '50%',
            text: 'ส่วนลด',
            style: 'summary',
          },
          {
            width: '50%',
            text: this.dataOrders.Total_discount.toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },
      {
        columns: [
          {
            width: '50%',
            text: 'ราคาสุทธิ',
            style: 'summary',
          },
          {
            width: '50%',
            text: (this.sumPrice - this.dataOrders.Total_discount).toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },
      {
        text: '----------------------------------------------------',
        style: 'item',
        alignment: 'center',
      },
      {
        columns: [
          {
            width: '50%',
            text: 'ราคาสินค้าไม่รวมภาษี',
            style: 'summary',
          },
          {
            width: '50%',
            text: (((this.sumPrice - this.dataOrders.Total_discount) * 100) / 107).toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },
      {
        columns: [
          {
            width: '50%',
            text: 'ภาษีมูลค่าเพิ่ม 7%',
            style: 'summary',
          },
          {
            width: '50%',
            text: (this.sumPrice - this.dataOrders.Total_discount - ((this.sumPrice - this.dataOrders.Total_discount) * 100) / 107).toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },

      {
        text: '----------------------------------------------------',
        style: 'item',
        alignment: 'center',
      },

      {
        columns: [
          {
            width: '50%',
            text: 'ค่ามัดจำสินค้า',
            style: 'summary',
          },
          {
            width: '50%',
            text: '',
            alignment: 'right',
            style: 'summary',
          },
        ],
      },

      {
        columns: [
          {
            width: '50%',
            text: 'รวม',
            style: 'summary',
          },
          {
            width: '50%',
            text: this.dataOrders.Total_deposit.toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },

      {
        columns: [
          {
            width: '50%',
            text: 'รวมราคาสุทธิ',
            style: 'summary',
          },
          {
            width: '50%',
            text: this.dataOrders.Total_amount.toFixed(2),
            alignment: 'right',
            style: 'summary',
          },
        ],
      },
      {
        text: ' ',
        style: 'item',
        alignment: 'center',
      },
      {
        text: ' ',
        style: 'item',
        alignment: 'center',
      },
      {
        text: ' ',
        style: 'item',
        alignment: 'center',
      },
      {
        text: ' ',
        style: 'item',
        alignment: 'center',
      },
      {
        text: '.',
        style: 'item',
        alignment: 'center',
      },
    ];

    // let contentAll = [
    //   {
    //     text: 'ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'VAT INCLUDE',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     image: logo,
    //     width: 80,
    //     alignment: 'center',
    //   },
    //   {
    //     text: companyAddress,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'สาขาที่ : -',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'เลขที่ : ' + this.dataOrders.Order_no,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'วันที่ : ' + moment(this.dataOrders.Order_date).format('DD/MM/YYYY HH:mm:ss'),
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'พนักงานขาย : (tentative)',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     layout: 'lightHorizontalLines',
    //     table: {
    //       headerRows: 1,
    //       widths: ['*', 'auto', '*', '*'],

    //       body: billbody,
    //     },
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคา',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคารวมสุทธิ',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคาไม่รวมภาษีมูลค่าเพิ่ม',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (
    //           this.dataOrders.Total_amount +
    //           this.dataOrders.Total_delivery -
    //           this.dataOrders.Total_discount -
    //           ((this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount) * 7) / 107
    //         ).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ภาษีมูลค่าเพิ่ม 7%',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (((this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount) * 7) / 107).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'เงินสด / เงินโอน',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: this.paymentType.type == '1' ? this.paymentType.cash.toFixed(2) : (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'เงินทอน',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text:
    //           this.paymentType.type == '1'
    //             ? (this.paymentType.cash - (this.dataOrders.Total_amount + this.dataOrders.Total_deposit + this.dataOrders.Total_delivery - this.dataOrders.Total_discount)).toFixed(2)
    //             : '0.00',
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   //------------------------------------------------------------------------------------------
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'ใบมัดจำถัง',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'VAT INCLUDE',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     image: logo,
    //     width: 80,
    //     alignment: 'center',
    //   },
    //   {
    //     text: companyAddress,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'สาขาที่ : -',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'เลขที่ : ' + this.dataOrders.Order_no,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'วันที่ : ' + moment(this.dataOrders.Order_date).format('DD/MM/YYYY HH:mm:ss'),
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'พนักงานขาย : (tentative)',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     layout: 'lightHorizontalLines',
    //     table: {
    //       headerRows: 1,
    //       widths: ['*', 'auto', '*', '*'],

    //       body: billDeposit,
    //     },
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคา',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: this.dataOrders.Total_deposit.toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคารวมสุทธิ',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: this.dataOrders.Total_deposit.toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: `"เงินประกันถังก๊าซหุงต้ม" ข้างต้นเป็นค่าประกันการยืมใช้ทรัพย์สินของบริษัท แอตลาส เอ็นเนอร์ยี่
    //     จำกัด ผู้บริโภคมีสิทธิ์ได้รับเงินประกันคืนเมื่อนำถังก๊าซหุงต้มพร้อมเอกสารฉบับนี้คืนบริษัทฯ โดย
    //     ถังก๊าซหุงต้มที่ผู้บริโภคนำมาคืน จะต้องไม่มีการชำรุดบกพร่องอันเกิดจากการใช้ถังก๊าซหุงต้ม
    //     อันผิดจากวิสัยของวิญญูชน`,
    //     // style: 'item',
    //     // alignment: 'center',
    //   },
    // ];

    // let contentBillOnly = [
    //   {
    //     text: 'ใบเสร็จรับเงิน/ใบกำกับภาษีอย่างย่อ',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'VAT INCLUDE',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     image: logo,
    //     width: 80,
    //     alignment: 'center',
    //   },
    //   {
    //     text: companyAddress,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'สาขาที่ : -',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'เลขที่ : ' + this.dataOrders.Order_no,
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'วันที่ : ' + moment(this.dataOrders.Order_date).format('DD/MM/YYYY HH:mm:ss'),
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: 'พนักงานขาย : (tentative)',
    //     style: 'header',
    //     alignment: 'center',
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     layout: 'lightHorizontalLines',
    //     table: {
    //       headerRows: 1,
    //       widths: ['*', 'auto', '*', '*'],

    //       body: billbody,
    //     },
    //   },
    //   {
    //     text: '----------------------------------------------------',
    //     style: 'item',
    //     alignment: 'center',
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคา',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคารวมสุทธิ',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ราคาไม่รวมภาษีมูลค่าเพิ่ม',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (
    //           this.dataOrders.Total_amount +
    //           this.dataOrders.Total_delivery -
    //           this.dataOrders.Total_discount -
    //           ((this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount) * 7) / 107
    //         ).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'ภาษีมูลค่าเพิ่ม 7%',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: (((this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount) * 7) / 107).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'เงินสด / เงินโอน',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text: this.paymentType.type == '1' ? this.paymentType.cash.toFixed(2) : (this.dataOrders.Total_amount + this.dataOrders.Total_delivery - this.dataOrders.Total_discount).toFixed(2),
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    //   {
    //     columns: [
    //       {
    //         width: '50%',
    //         text: 'เงินทอน',
    //         style: 'summary',
    //       },
    //       {
    //         width: '50%',
    //         text:
    //           this.paymentType.type == '1'
    //             ? (this.paymentType.cash - (this.dataOrders.Total_amount + this.dataOrders.Total_deposit + this.dataOrders.Total_delivery - this.dataOrders.Total_discount)).toFixed(2)
    //             : '0.00',
    //         alignment: 'right',
    //         style: 'summary',
    //       },
    //     ],
    //   },
    // ];

    // if (billDeposit.length > 0) {
    //   content = contentAll;
    // } else {
    //   content = contentBillOnly;
    // }

    const docDef = {
      pageSize: {
        width: 264.57,
        height: 'auto',
      },
      pageMargins: [10, 10, 10, 10],
      content: content,
      styles: {
        header: {
          fontSize: 10,
          bold: true,
          alignment: 'center',
        },
        item: {
          fontSize: 10,
          bold: false,
          alignment: 'center',
        },
        summary: {
          fontSize: 10,
          bold: false,
        },
        footer: {
          fontSize: 10,
          bold: true,
          alignment: 'left',
        },
      },
      defaultStyle: {
        font: 'Kanit',
        fontSize: 8,
      },
    };

    // console.log(docDef);

    this.pdfObj = pdfMake.createPdf(docDef);
    // create pdf

    if (this.plt.is('cordova')) {
      // if cordova / device android or ios
      this.pdfObj.getBase64(async (data) => {
        try {
          const path = `pt-slip/${this.dataOrders.Order_no}.pdf`;

          const result = await Filesystem.writeFile({
            path,
            data: data,
            directory: FilesystemDirectory.Documents,
            recursive: true,
          });

          // this.fileOpener.open(`${result.uri}`, 'application/pdf');

          this.fileOpener
            .open(`${result.uri}`, 'application/pdf')
            .then(() => console.log('File is opened'))
            .catch((e) => console.log('Error opening file', e));

          const alert = await this.alertCtrl.create({
            header: 'บันทึกใบเสร็จ',
            message: 'บันทึกใบเสร็จรับเงินเรียบร้อย',
            cssClass: 'custom-modal',
            buttons: [
              // {
              //   text: 'ไม่ใช่',
              //   handler: () => {},
              // },
              {
                text: 'ตกลง',
                handler: () => {},
              },
            ],
            backdropDismiss: false, // <- Here! :)
          });
          await alert.present();
        } catch (error) {
          console.error(error);
        }
      });
    } else {
      // if web or Pwa
      this.pdfObj.download(this.dataOrders.Sale_no + '.pdf').open();
    }
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
    });
    toast.present();
  }
}
