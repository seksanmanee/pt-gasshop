import { Component, OnInit } from '@angular/core';

import { AlertController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { StorageService } from './storage.service';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { AppVersion } from '@awesome-cordova-plugins/app-version/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'หน้าหลัก',
      url: '/tabs',
      icon: 'home',
    },
    {
      title: 'เข้าสู่ระบบ',
      url: '/login',
      icon: 'paper-plane',
    },
  ];

  appVersionText;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public alertCtrl: AlertController,
    public storageService: StorageService,
    private screenOrientation: ScreenOrientation,
    private appVersion: AppVersion
  ) {
    this.initializeApp();
  }

  initializeApp() {
    // this.platform.ready().then(() => {
    //   this.storageService.getObject('logined').then(result => {
    //     if (result != null) {
    //       this.router.navigateByUrl('/tabs');
    //     }else{
    //       this.router.navigateByUrl('/login');
    //     }
    //   }).catch(e => {
    //     this.router.navigateByUrl('/login');
    //   });
    //   this.statusBar.styleDefault();
    //   this.splashScreen.hide();
    // });
    // this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    //
    this.appVersion.getAppName();
    this.appVersion.getPackageName();
    this.appVersion.getVersionCode();
    this.appVersionText = this.appVersion.getVersionNumber();

    console.log(this.appVersionText);
  }

  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });
    alert.present();
  }

  ngOnInit() {
    const path = window.location.pathname.split('/')[1];
    console.log(path);
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex((page) => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'ยืนยัน!',
      message: 'คุณต้องการออกจากระบบใช่หรือไม่ ?',
      cssClass: 'custom-modal',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          },
        },
        {
          text: 'ยืนยัน',
          handler: () => {
            this.storageService.clear();
            this.router.navigate(['/login']);
          },
        },
      ],
      backdropDismiss: false, // <- Here! :)
    });

    await alert.present();
  }
  async querySelectorAll() {}
}
